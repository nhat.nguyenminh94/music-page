<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::group(['prefix'=>'song','as'=>'song.'],function(){
    	Route::get('/', 'SongController@create')->name('create');
    	Route::post('store', 'SongController@store')->name('store');
    	Route::post('/uploadfile/{type?}', 'SongController@uploadFile')->name('uploadfile');
    });
});