@extends('admin.layouts.master')
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Tạo bài hát</h1>
</div>
<div class="container">
	<div class="card shadow col-md-12">
		<form action="{{route('admin.song.store')}}" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputEmail4">Tiêu Đề</label>
					<input type="text" class="form-control" name="title" id="" placeholder="Tiêu Đề">
				</div>
				<div class="form-group col-md-6">
					<label for="inputPassword4">Ca sĩ</label>
					<input type="text" class="form-control" name="singer" id="inputPassword4" placeholder="ca sĩ">
				</div>
			</div>
			<div class="form-group">
				<label for="inputAddress">Nhạc sĩ</label>
				<input type="text" class="form-control" name="musician" id="inputAddress" placeholder="Nhac sĩ">
			</div>
			<div class="form-group">
				<label for="inputAddress2">Tag</label>
				<input type="text" class="form-control" name="tag" id="inputAddress2" placeholder="">
			</div>
			<div class="form-group file-form">
				<label for="inputAddress2">File nhạc</label>
				<input type="file" multiple  onchange="UploadFile(this)"  class="form-control" name="UploadFile[]" id="inputAddress2" placeholder="">
				<div class="row col-md-12">
					<div class="progress">
						<div class="progress-bar progress-bar-striped progress-bar-animated progressBarUploadFile" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
							<span
							class="progressBarUploadFileShow">0%</span>

						</div>
					</div>
				</div>
				<div class="row col-md-12 form-file">
					
				</div>
			</div>
			<div class="form-group ">
				<label for="inputCity">Trạng thái</label>
				<div class="row col-md-12">
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1">
						<label class="form-check-label" for="inlineRadio1">Ẩn</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="2">
						<label class="form-check-label" for="inlineRadio2">Hiện</label>
					</div>
				</div>

			</div>
			<button type="submit" class="btn btn-primary">Tạo</button>
		</form>
	</div>
</div>
@endsection
@section('scripts')
	<script type="text/javascript">
		 function UploadFile(e) {
            var form_data = new FormData();
            $.each( e.files, function( key, value ) {
		 		form_data.append("files[]", value);
			});
			form_data.append('_token', "{{ csrf_token() }}");
			 $.ajax({
                url: "{{route('admin.song.uploadfile',['type'=>'song'])}}",
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                progress: function (e) {
                	console.log(e);
                 if (e.lengthComputable) {
                        var pct = (e.loaded / e.total) * 100;
                        $('.progressBarUploadFile').attr('aria-valuenow', parseInt(pct)).css('width', parseInt(pct) + '%');
                        $('.progressBarUploadFileShow').text(parseInt(pct) + '%');
                        if (parseInt(pct) == 100) {
                            $('.progressBarUploadFileShow').text('complete');
                        }
                    }
                },
                success: function (res) {
                	// console.log(res.dataDone);
                 //  if(jQuery.isEmptyObject(res.dataFail) == true){
                 //  	// $('.row-file').append('<div class="row">')
                 //  }
                  if(jQuery.isEmptyObject(res.dataDone) == false){
                  	res.dataDone.forEach(function (element) {
                  		// console.log(element.filename);
                  		$('.form-file').append('<div class="row col-md-12">'
                  			+'<audio controls>'
                  			+' <source src="/' + element.filename + '" type="audio/mpeg">'
                  			+'</audio>'
                  			+'</div>'
                  			);
                  	});
                  	// $('.row-file').append('<div class="row">')
                  }
                }, error: function (error) {
                   // console.log(error);
                }
            });


        }
	</script>
@endsection