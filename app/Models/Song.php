<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
	  protected $fillable=['title','singer','detail','musician','status','tag'];
      public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

}
