<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['src', 'alt'];
    public function filetable()
    {
        return $this->morphTo();
    }

}
