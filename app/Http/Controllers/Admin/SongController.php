<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use File;
class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.song.Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

      public function uploadFile(Request $request, $type=null)
    {

       $dataDone = [];
       $dataFail = [];

        foreach ($request->file('files') as $key => $file) {
            $validator = Validator::make($request->all(),  [
                'file' => 'mimetypes:audio/mpeg',
            ],
            [
                'file.mimetypes' => 'file phải là âm thanh  (mp3, mp4)',
            ]);

            if ($validator->fails()) {
                $dataUpload[] =  [
                    'status' => false,
// 'file' => $request->file('files')[$key]->getClientOriginalName() ?? null,
                    'errors' => $validator->errors()->getMessages()['file'],
                ];
            }else{
                if ($type) {
                    $dir = 'uploads/' . $type . '/';
                } else {
                    $dir = 'uploads/';
                }

                $date = date("dmY");
                $originName = $file->getClientOriginalName();
                $file_name = rand() . $date . '.' . $file->getClientOriginalExtension();
                if ($file->move($dir, $file_name)) {
                    $dataUpload[] = ['status' => true, 'filename' => $dir . $file_name, 'filenameorigin' => $originName];
                }else{
                    $dataUpload[] =  ['status' => false, 'errors' => $originName];  
                }
            }
        }
    foreach($dataUpload as $val) {
         if ($val['status'] == false) {
        $dataFail[] = $val;
    }
    if ($val['status'] == true) {
        $dataDone[] = $val;
    }
    }  
   
    return [
        'dataDone' =>     $dataDone,
        'dataFail' =>     $dataFail,
    ];
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
